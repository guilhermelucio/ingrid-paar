import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';

//Sections
import { HomeComponent } from './sections/home/home.component';
import { ServicesComponent } from './sections/services/services.component';
import { TeamComponent } from './sections/team/team.component';
import { ClientsComponent } from './sections/clients/clients.component';
import { NewsComponent } from './sections/news/news.component';
import { BlogComponent } from './sections/blog/blog.component';
import { ContactComponent } from './sections/contact/contact.component';
import { PageNotFoundComponent } from './pageNotFound/page.not.found.component';

//commons
import { HeaderComponent } from './commons/header/header.component';
import { FooterComponent } from './commons/footer/footer.component';
import { SidebarModule } from 'ng-sidebar';
import { SidemenuComponent } from './commons/sidemenu/sidemenu.component';
import { Ng2ParallaxScrollModule } from 'ng2-parallax-scroll';
import { NgsRevealModule } from 'ng-scrollreveal';
import { BeintouchComponent } from './commons/beintouch/beintouch.component';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { ClientComponent } from './commons/client/client.component';
import { ModalModule } from 'ng2-bootstrap';
import { Ng2PageScrollModule } from 'ng2-page-scroll';
import { ButtonIconComponent } from './commons/buttonIcon/buttonIcon.component';

//Routing
import { ROUTING } from './commons/routes/routes';

@NgModule({
  imports:      [ 
    BrowserModule, 
    ROUTING,
    Ng2ParallaxScrollModule,
    NgsRevealModule.forRoot(),
    SidebarModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDHllF-N3L4Y9J1cJayojkRJ2h4KthWRkQ'
    }),
    ModalModule.forRoot(),
    Ng2PageScrollModule.forRoot()
  ],
  declarations: [ 
    AppComponent, 
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    ServicesComponent,
    TeamComponent,
    ButtonIconComponent,
    ClientsComponent,
    NewsComponent,
    BlogComponent,
    ContactComponent,
    SidemenuComponent,
    BeintouchComponent,
    ClientComponent,

  ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
