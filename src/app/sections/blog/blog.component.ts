import { Component } from '@angular/core';
import { MasonryOptions } from 'angular2-masonry';

@Component({
  selector: 'blog-component',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})

export class BlogComponent {
  title = 'app works!';
  bricks = [
     {id: 0, title: 'Cambios en los plazos de reporte de medios magnéticos', url_img: '../assets/images/blog/2017/03/01/blog-01.jpg', class:'hide-item'},
     {id: 1, title: 'Cambios en e medios magnéticos', url_img: '../assets/images/blog/2017/03/01/blog-02.jpg', class:'hide-item'},
   ]

	public myOptions: MasonryOptions;any = { 
		itemSelector: '.brick',
		columnWidth: 180,
  	isFitWidth: true,
	};

	toggle_class_on(id:number){
		this.bricks[id].class = 'show-item'; 
  }
	toggle_class_off(id:number){
		this.bricks[id].class = 'hide-item'; 
  }
}