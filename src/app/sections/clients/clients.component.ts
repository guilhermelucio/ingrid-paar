import { Component, Input } from '@angular/core';

@Component({
  selector: 'clients-component',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})

export class ClientsComponent {
  //Variables
  @Input() backgroundColorMenu:string;  // Background color
  items = [
            { id: 1, 
              name: 'Natural LED', 
              category: "Electrodomesticoss",
              slug: "cliente-1", 
              img_url: '../assets/images/clients/03.jpg', 
              desc: "Natural LED es una empresa familiar ubicada en Cali - Valle del Cauca, somos expertos en iluminación LED, tecnología muy eficiente y amigable con el medio ambiente. Contamos con dos sucursales, una en el centro de la ciudad y otra en el sur de Cali, atenderlo con el corazón es nuestra promesa de valor.",
              facebook: "https://www.facebook.com/naturalledcolombia/",
              instagram: "https://www.facebook.com/naturalledcolombia/",
              youtube: "https://www.facebook.com/naturalledcolombia/",
              linkedin: "https://www.facebook.com/naturalledcolombia/",
              web: "https://www.facebook.com/naturalledcolombia/",
           },
            { id: 2, 
              name: 'Natural LED',
              category: "Electrodomesticoss", 
              slug: "cliente-2", 
              img_url: '../assets/images/clients/05.jpg', 
              facebook: "https://www.facebook.com/naturalledcolombia/",
              linkedin: "https://www.facebook.com/naturalledcolombia/",
              web: "https://www.facebook.com/naturalledcolombia/",
            },
            { id: 3, 
              name: 'Natural LED', 
              category: "Electrodomesticoss",
              slug: "cliente-3", 
              img_url: '../assets/images/clients/02.jpg', 
              linkedin: "https://www.facebook.com/naturalledcolombia/",
              web: "https://www.facebook.com/naturalledcolombia/",
            },
            { id: 4, 
              name: 'Natural LED', 
              category: "Electrodomesticoss",
              slug: "cliente-4", 
              img_url: '../assets/images/clients/03.jpg', 
            },
            { id: 5, 
              name: 'Natural LED', 
              category: "Electrodomesticoss",
              slug: "cliente-5", 
              img_url: '../assets/images/clients/04.png', 
            },
            { id: 6, 
              name: 'Natural LED', 
              category: "Electrodomesticoss",
              slug: "cliente-6", 
              img_url: '../assets/images/clients/05.jpg', 
            },
            { id: 7, 
              name: 'Natural LED', 
              category: "Electrodomesticoss",
              slug: "cliente-7", 
              img_url: '../assets/images/clients/04.png', 
            },
            { id: 8, 
              name: 'Natural LED', 
              category: "Electrodomesticoss",
              slug: "cliente-8", 
              img_url: '../assets/images/clients/02.jpg', 
            },
          ];

  constructor(){}
  
}
