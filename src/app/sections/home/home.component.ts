import { Component } from '@angular/core';

// import { HeaderComponent } from './header/header.component'

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent {
  title = 'app works!';

  //declare a variable to hold class name:
  public my_Class1 = 'plus-one';
  public my_Class2 = 'plus-one';
  public my_Class3 = 'plus-one';
  public my_Class4 = 'plus-one';

  toggle_class_arrow(classIcon:string){
    switch(classIcon){
      case 'one':
      this.my_Class1='arrow';
      break;
      case 'two':
      this.my_Class2='arrow';
      break;
      case 'three':
      this.my_Class3='arrow';
      break;
      case 'four':
      this.my_Class4='arrow';
      break;
    }
  }

  toggle_class_plus(classIcon:string){
    switch(classIcon){
      case 'one':
      this.my_Class1='plus-one';
      break;
      case 'two':
      this.my_Class2='plus-one';
      break;
      case 'three':
      this.my_Class3='plus-one';
      break;
      case 'four':
      this.my_Class4='plus-one';
      break;
    }
  }

}
