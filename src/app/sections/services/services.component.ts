import { Component } from '@angular/core';

// import { HeaderComponent } from './header/header.component'

@Component({
  selector: 'services-component',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})

export class ServicesComponent {
  title = 'app works!';
  showOne:boolean;
  showTwo:boolean;
  showThree:boolean;
  showFour:boolean;
  url:string = window.location.href;

  constructor(){
    if(this.url.indexOf("consultoria-contable") !== -1){
      this.showOne = true;
    }
    if(this.url.indexOf("sistemas-de-calidad") !== -1){
      this.showTwo = true;
    }
    if(this.url.indexOf("desarrollo-web") !== -1){
      this.showThree = true;
    }
    if(this.url.indexOf("marketing-digital") !== -1){
      this.showFour = true;
    }
  }
  
  
  
}
