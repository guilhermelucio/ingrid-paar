import { Component } from '@angular/core';

// import { HeaderComponent } from './header/header.component'

@Component({
  selector: 'clients-component',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent {
  title = 'app works!';
  lat: number = 3.432330;
  lng: number = -76.545270;
  showMap = false; 
}
