import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template:`
        <router-outlet></router-outlet>
        <footer-component></footer-component>   
   `
})
export class AppComponent  { name = 'Angular'; }
  