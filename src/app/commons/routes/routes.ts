/* importando modulos necesarios paa el enrrutamiento  */
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* importando Components  */
import { HomeComponent }        from '../../sections/home/home.component';
import { ServicesComponent }        from '../../sections/services/services.component';
import { TeamComponent }        from '../../sections/team/team.component';
import { ClientsComponent }        from '../../sections/clients/clients.component';
import { NewsComponent }        from '../../sections/news/news.component';
import { BlogComponent }        from '../../sections/blog/blog.component';
import { ContactComponent }        from '../../sections/contact/contact.component';
import { PageNotFoundComponent }        from '../../pageNotFound/page.not.found.component';

export const ROUTES: Routes = [
    {
      path: '',
      component: HomeComponent
    },
    {
      path: 'servicios/consultoria-contable',
      component: ServicesComponent
    },
    {
      path: 'servicios/sistemas-de-calidad',
      component: ServicesComponent
    },
    {
      path: 'servicios/desarrollo-web',
      component: ServicesComponent
    },
    {
      path: 'servicios/marketing-digital',
      component: ServicesComponent
    },
    {
      path: 'equipo',
      component: TeamComponent
    },
    {
      path: 'aliados',
      component: ClientsComponent
    },
    {
      path: 'contacto',
      component: ContactComponent
    },
    {
      path: 'blog',
      component: BlogComponent
    },
    {
      path: '**',
      component: PageNotFoundComponent
    },
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES);