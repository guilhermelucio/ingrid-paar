import { Component, Input } from '@angular/core';

@Component({
  selector: 'items-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})

export class ClientComponent {
  //Variables
  @Input() items:any[];            // Array de items
  
}