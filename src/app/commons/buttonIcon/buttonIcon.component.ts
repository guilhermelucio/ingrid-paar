import { Component, Input } from '@angular/core';

@Component ({
    selector: 'buttonIcon',
    templateUrl : './buttonIcon.component.html',
    styleUrls : ['./buttonIcon.component.css']
})

export class ButtonIconComponent {
  
  @Input() colorIcon:string; // Color Icon
  @Input() classCol:string;  // Clase color
  @Input() url:string;       // Url button 
  @Input() title:string;     // Title button

  //declare a variable to hold class name:
  public my_Class1 = 'plus-one';

  toggle_class_arrow(classIcon:string){
    this.my_Class1='arrow';
  }

  toggle_class_plus(classIcon:string){
    this.my_Class1='plus-one';
  }
}