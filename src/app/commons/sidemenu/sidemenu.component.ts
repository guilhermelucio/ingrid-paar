import { Component, Input, AnimationTransitionEvent } from '@angular/core';

@Component({
  selector: 'side-menu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})

export class SidemenuComponent {
  private _opened: boolean = false;
  private _modeNum: number = 0;
  private _positionNum: number = 1;
  private _closeOnClickOutside: boolean = true;
  private _showBackdrop: boolean = true;
  private _animate: boolean = true;
  private _trapFocus: boolean = true;
  private _autoFocus: boolean = true;
  private _keyClose: boolean = false;

  private _MODES: Array<string> = ['over', 'push', 'dock'];
  private _POSITIONS: Array<string> = ['left', 'right', 'top', 'bottom'];

  private _toggleOpened(): void {
    this._opened = !this._opened;
  }

  private _toggleMode(): void {
    this._modeNum++;

    if (this._modeNum === this._MODES.length) {
      this._modeNum = 0;
    }
  }

  private _togglePosition(): void {
    this._positionNum++;

    if (this._positionNum === this._POSITIONS.length) {
      this._positionNum = 0;
    }
  }

  private _toggleCloseOnClickOutside(): void {
    this._closeOnClickOutside = !this._closeOnClickOutside;
  }

  private _toggleShowBackdrop(): void {
    this._showBackdrop = !this._showBackdrop;
  }

  private _toggleAnimate(): void {
    this._animate = !this._animate;
  }

  private _toggleTrapFocus(): void {
    this._trapFocus = !this._trapFocus;
  }

  private _toggleAutoFocus(): void {
    this._autoFocus = !this._autoFocus;
  }

  private _toggleKeyClose(): void {
    this._keyClose = !this._keyClose;
  }

  private _onOpenStart(): void {
    // console.info('Sidebar opening');
  }

  private _onOpened(): void {
    // console.info('Sidebar opened');
  }

  private _onCloseStart(): void {
    // console.info('Sidebar closing');
  }

  private _onClosed(): void {
    // console.info('Sidebar closed');
  }

  private _onAnimationStart(e: AnimationTransitionEvent): void {
    // console.info('Animation start', e);
  }

  private _onAnimationDone(e: AnimationTransitionEvent): void {
    // console.info('Animation done', e);
  }

  //Mine
  @Input() menus: any[];            // Tipo de logo
  @Input() logo: any[];            // Tipo de logo
  @Input() logoType:string;             // Tipo de logo
}
