import { Component, Input } from '@angular/core';

@Component ({
    selector: 'main-header',
    templateUrl : './header.component.html',
    styleUrls : ['./header.component.css'],
    host: {
        '(window:scroll)': 'updateHeader($event)'
    }
})

export class HeaderComponent {

    //Variables
    @Input() backgroundColorMenu:string;  // Background color
    @Input() textFont:string;             // Font family
    @Input() sizeFont:number;             // Font size
    @Input() colorButton:string;          // Color family
    @Input() colorMenu:string;            // Color font
    @Input() colorMenuB:string;            // Color font
    @Input() colorHoverMenu:string;       // Color font
    @Input() colorHoverMenuB:string;       // Color font
    @Input() logoType:string;             // Tipo de logo
    @Input() logoTypeSecond:string;             // Tipo de logo
    @Input() position:string;             // Posicion del nav

    //Variables scroll header
    isScrolled = false;
    currPos: Number = 0;
    startPos: Number = 0;
    changePos: Number = 100;

    //Method to detect if the user did scroll
    updateHeader(evt:any) {
        this.currPos = (window.pageYOffset || evt.target.scrollTop) - (evt.target.clientTop || 0);
        if(this.currPos >= this.changePos ) {
            this.isScrolled = true;
        } else {
            this.isScrolled = false;
        }
    }

    //Json Logo
    logo = {
            original: './assets/images/logo/original.png',
            black: './assets/images/logo/black.png',
            white: './assets/images/logo/white.png',
            violet: './assets/images/logo/violet.png',
            blue: './assets/images/logo/blue-green.png',
            label: 'Grupo Paar'
           }

    //Json Menu
    menus = [
                { label: 'Servicios', slug: 'servicios/consultoria-contable'},
                { label: 'Contacto', slug: 'contacto'},
            ]

    constructor(){
    }
}