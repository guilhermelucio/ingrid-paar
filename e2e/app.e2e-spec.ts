import { AngularCliPaarPage } from './app.po';

describe('angular-cli-paar App', function() {
  let page: AngularCliPaarPage;

  beforeEach(() => {
    page = new AngularCliPaarPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
